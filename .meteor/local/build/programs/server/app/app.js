var require = meteorInstall({"both":{"router":{"config.js":function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// both/router/config.js                                             //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
Router.configure({                                                   // 1
    loadingTemplate: 'loading',                                      // 2
    layoutTemplate: 'mainLayout',                                    // 3
    notFoundTemplate: 'notFound'                                     // 4
});                                                                  //
///////////////////////////////////////////////////////////////////////

},"hooks.js":function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// both/router/hooks.js                                              //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
var routesForUnloggedUsers = [                                       // 1
    // 'login',                                                      //
    // 'register',                                                   //
];                                                                   //
                                                                     //
var isLoggedIn = function isLoggedIn() {                             // 6
    var user = Meteor.user();                                        // 7
    if (!user && !Meteor.loggingIn()) {                              // 8
        Router.go('login');                                          // 9
    } else {                                                         //
        this.next();                                                 // 12
    }                                                                //
};                                                                   //
                                                                     //
// Router.onBeforeAction(isLoggedIn, {                               //
//     except: routesForUnloggedUsers                                //
// });                                                               //
///////////////////////////////////////////////////////////////////////

},"routes.js":function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// both/router/routes.js                                             //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
Router.map(function () {                                             // 1
    this.route('home', {                                             // 2
        path: '/',                                                   // 3
        template: 'home'                                             // 4
    });                                                              //
});                                                                  //
///////////////////////////////////////////////////////////////////////

}}}},{"extensions":[".js",".json"]});
require("./both/router/config.js");
require("./both/router/hooks.js");
require("./both/router/routes.js");
//# sourceMappingURL=app.js.map
