var require = meteorInstall({"client":{"layouts":{"loading":{"template.loading.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/layouts/loading/template.loading.js                              //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           // 1
Template.__checkName("loading");                                           // 2
Template["loading"] = new Template("Template.loading", (function() {       // 3
  var view = this;                                                         // 4
  return "";                                                               // 5
}));                                                                       // 6
                                                                           // 7
/////////////////////////////////////////////////////////////////////////////

},"loading.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/layouts/loading/loading.js                                       //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           //
/////////////////////////////////////////////////////////////////////////////

}},"mainLayout":{"template.main-layout.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/layouts/mainLayout/template.main-layout.js                       //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           // 1
Template.__checkName("mainLayout");                                        // 2
Template["mainLayout"] = new Template("Template.mainLayout", (function() {
  var view = this;                                                         // 4
  return Spacebars.include(view.lookupTemplate("yield"));                  // 5
}));                                                                       // 6
                                                                           // 7
/////////////////////////////////////////////////////////////////////////////

},"main-layout.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/layouts/mainLayout/main-layout.js                                //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           //
/////////////////////////////////////////////////////////////////////////////

}},"notFound":{"template.not-found.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/layouts/notFound/template.not-found.js                           //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           // 1
Template.__checkName("notFound");                                          // 2
Template["notFound"] = new Template("Template.notFound", (function() {     // 3
  var view = this;                                                         // 4
  return HTML.Raw("<h1>Not found</h1>");                                   // 5
}));                                                                       // 6
                                                                           // 7
/////////////////////////////////////////////////////////////////////////////

},"not-found.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/layouts/notFound/not-found.js                                    //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           //
/////////////////////////////////////////////////////////////////////////////

}}},"views":{"home":{"template.home.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/views/home/template.home.js                                      //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           // 1
Template.__checkName("home");                                              // 2
Template["home"] = new Template("Template.home", (function() {             // 3
  var view = this;                                                         // 4
  return HTML.Raw("<p> This is homepage </p>");                            // 5
}));                                                                       // 6
                                                                           // 7
/////////////////////////////////////////////////////////////////////////////

},"home.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// client/views/home/home.js                                               //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
                                                                           //
/////////////////////////////////////////////////////////////////////////////

}}}},"both":{"router":{"config.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// both/router/config.js                                                   //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
Router.configure({                                                         // 1
    loadingTemplate: 'loading',                                            // 2
    layoutTemplate: 'mainLayout',                                          // 3
    notFoundTemplate: 'notFound'                                           // 4
});                                                                        //
/////////////////////////////////////////////////////////////////////////////

},"hooks.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// both/router/hooks.js                                                    //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
var routesForUnloggedUsers = [                                             // 1
    // 'login',                                                            //
    // 'register',                                                         //
];                                                                         //
                                                                           //
var isLoggedIn = function isLoggedIn() {                                   // 6
    var user = Meteor.user();                                              // 7
    if (!user && !Meteor.loggingIn()) {                                    // 8
        Router.go('login');                                                // 9
    } else {                                                               //
        this.next();                                                       // 12
    }                                                                      //
};                                                                         //
                                                                           //
// Router.onBeforeAction(isLoggedIn, {                                     //
//     except: routesForUnloggedUsers                                      //
// });                                                                     //
/////////////////////////////////////////////////////////////////////////////

},"routes.js":function(){

/////////////////////////////////////////////////////////////////////////////
//                                                                         //
// both/router/routes.js                                                   //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
                                                                           //
Router.map(function () {                                                   // 1
    this.route('home', {                                                   // 2
        path: '/',                                                         // 3
        template: 'home'                                                   // 4
    });                                                                    //
});                                                                        //
/////////////////////////////////////////////////////////////////////////////

}}}},{"extensions":[".js",".json",".html"]});
require("./client/layouts/loading/template.loading.js");
require("./client/layouts/mainLayout/template.main-layout.js");
require("./client/layouts/notFound/template.not-found.js");
require("./client/views/home/template.home.js");
require("./client/layouts/loading/loading.js");
require("./client/layouts/mainLayout/main-layout.js");
require("./client/layouts/notFound/not-found.js");
require("./client/views/home/home.js");
require("./both/router/config.js");
require("./both/router/hooks.js");
require("./both/router/routes.js");