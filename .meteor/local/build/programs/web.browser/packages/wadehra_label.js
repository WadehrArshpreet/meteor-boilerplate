//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
// Source maps are supported by all recent versions of Chrome, Safari,  //
// and Firefox, and by Internet Explorer 11.                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var global = Package.meteor.global;
var meteorEnv = Package.meteor.meteorEnv;
var $ = Package.jquery.$;
var jQuery = Package.jquery.jQuery;
var meteorInstall = Package.modules.meteorInstall;
var Buffer = Package.modules.Buffer;
var process = Package.modules.process;
var Symbol = Package['ecmascript-runtime'].Symbol;
var Map = Package['ecmascript-runtime'].Map;
var Set = Package['ecmascript-runtime'].Set;
var meteorBabelHelpers = Package['babel-runtime'].meteorBabelHelpers;
var Promise = Package.promise.Promise;

var require = meteorInstall({"node_modules":{"meteor":{"wadehra:label":{"az-label.js":function(){

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/wadehra_label/az-label.js                                //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
// Write your package code here!                                     //
if (Meteor.isClient) {                                               // 2
    var drawAZLogo = function drawAZLogo() {                         //
        var logo = "__      __       _     _         _          _                     _   \r\n\\ \\    / /_ _ __| |___| |_  _ _ /_\\  _ _ __| |_  _ __ _ _ ___ ___| |_ \r\n \\ \\/\\/ / _` / _` / -_) ' \\| '_/ _ \\| '_(_-< ' \\| '_ \\ '_/ -_) -_)  _|\r\n  \\_/\\_/\\__,_\\__,_\\___|_||_|_|/_/ \\_\\_| /__/_||_| .__/_| \\___\\___|\\__|\r\n                                                |_|                   \r\n  MeteorJS/PHP/Android Developer LinkedIn@WadehrArshpreet";
                                                                     //
        $('html').prepend('<!--' + logo + '-->');                    // 7
        console.log(logo);                                           // 8
    };                                                               //
                                                                     //
    drawAZLogo();                                                    // 10
}                                                                    //
///////////////////////////////////////////////////////////////////////

}}}}},{"extensions":[".js",".json"]});
require("./node_modules/meteor/wadehra:label/az-label.js");

/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['wadehra:label'] = {};

})();
